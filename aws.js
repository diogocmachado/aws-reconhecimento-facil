var AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: process.env.ACCESSKEYID,
    secretAccessKey: process.env.SECRETACCESSKEY,
    region: process.env.REGION
});

module.exports = {
    criar: function(){
        return new AWS.Rekognition();
    }
}