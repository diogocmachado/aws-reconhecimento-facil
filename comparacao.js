const aws = require('./aws');
const imagem = require('./imagem');


var params = {
    SimilarityThreshold: 90,
    SourceImage: {
        Bytes: imagem.getImagemBuffer('foto1.jpg')
    }, 
    TargetImage: {
        Bytes: imagem.getImagemBuffer('foto2.jpg')
    }
};

aws.criar().compareFaces(params, function (err, data) {
  if (err) console.log(err, err.stack); 
  else     console.log(printResult(data));//console.log(JSON.stringify(data));
});

function printResult(data){
    var faceMatches = (data.FaceMatches ? data.FaceMatches[0] : undefined);

    if(faceMatches){
       console.log('Grau de confianca: ' + faceMatches.Face.Confidence);
       console.log('Similaridade: ' + faceMatches.Similarity);
    }else {
        console.log('As pessoas são diferentes.');
    }
}