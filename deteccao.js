const aws = require('./aws');
const imagem = require('./imagem');


var params = {
    Image: { 
      Bytes: imagem.getImagemBuffer("foto3.jpg")
    },
    Attributes: ['ALL']
  };

aws.criar().detectFaces(params, function(err, data) {
    if (err) console.log(err);
    else     console.log(printResult(data));
});

function printResult(data){
    var faceDetail = data.FaceDetails[0];
    var emocao = faceDetail.Emotions[0];
    var genero = faceDetail.Gender;
    var oculos = faceDetail.Eyeglasses;
    var oculosEscuros = faceDetail.Sunglasses;
    var barba = faceDetail.Beard;
    var bigode = faceDetail.Mustache;

    console.log("Grau de confiança: " + faceDetail.Confidence);
    console.log("Range de Idade: " + faceDetail.AgeRange.Low + ' - '+faceDetail.AgeRange.High);
    console.log("Emocao: " + emocao.Type);
    console.log("Gênero: " + genero.Value);
    console.log("Óculos: " + (oculos.Value ? "Sim" : "Não"));
    console.log("Óculos Escuros: " + (oculosEscuros.Value ? "Sim" : "Não"));
    console.log("Barba: " + (barba.Value ? "Sim" : "Não"));
    console.log("Bigode: " + (bigode.Value ? "Sim" : "Não"));

}