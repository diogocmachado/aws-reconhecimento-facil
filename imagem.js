const fs = require('fs');

const REPO_IMAGEM = process.env.REPO_IMAGENS; 

module.exports = {
    getImagemBuffer : function (imagem){
        var file = fs.readFileSync(REPO_IMAGEM + imagem);
        return new Buffer(file);
    }
}